var gulp = require('gulp');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var stripDebug = require('gulp-strip-debug');
var webserver = require('gulp-webserver');
var jade = require('gulp-jade');

gulp.task('default', function() {
    gulp.src('src/**/*.jade')
        .pipe(jade({}))
        .pipe(gulp.dest('public'));

    return gulp.src('src/**/*.js')
        .pipe(babel())
        //    .pipe(stripDebug())
        .pipe(uglify())
        .pipe(concat('build.js'))
        .pipe(gulp.dest('public/build'));
});

gulp.watch('src/**/*.js', ['default']);
gulp.watch('src/**/*.jade', ['default']);

gulp.task('webserver', function() {
    gulp.src('public')
        .pipe(webserver({
            livereload: true,
            directoryListing: false,
            open: true,
            port: 8080
        }));
});
gulp.start('webserver');
