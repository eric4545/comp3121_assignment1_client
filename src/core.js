var apiUrl = "https://comp3121-ass1-polyu-14005194d.c9.io/api/news/:newsId/vote";
var curUser = {};
var newsId;

window.fbAsyncInit = function() {
    FB.init({
        appId: '517354425094676',
        xfbml: true,
        status: true,
        cookie: true,
        version: 'v2.5'
    });
    FB.Canvas.setAutoGrow();
    FB.Event.subscribe("auth.login", checkFBAuth);
    FB.Event.subscribe("auth.logout", checkFBAuth);
    checkLoginState();
};

function checkLoginState() {
    FB.getLoginStatus(function(response) {
        checkFBAuth(response);
    });
}

function checkFBAuth(response) {
    switch (response.status) {
        case 'connected':
            curUser.uid = response.authResponse.userID;
            curUser.accessToken = response.authResponse.accessToken;
            break;
        default: // the user isn't logged in to Facebook.
        case 'not_authorized':
            // the user is logged in to Facebook,
            // but has not authenticated your app
            delete sessionStorage.user;
            delete curUser.uid;
            delete curUser.accessToken;
            break;
    }
}
