var commentStyle = {
    commentBox: {
        flex: 1
    },
    comment: {
        flex: 1
    }
}

var comments = [
    {
        "author": "Eric",
        "comment": "Helo"
    }, {
        "author": "A",
        "comment": "B"
    }, {
        "author": "C",
        "comment": "D"
    }
]

var CommentBox = React.createClass({
    fetchCommentList: function() {
        var self = this;
        $.ajax({
            type: "GET",
            url: apiUrl.replace(':newsId', newsId)
                .replace('/vote', ''),
            success: function(data) {
                console.log(data);

                //self.setState(state);
            }
        });
    },
    componentDidMount: function() {
        this.fetchCommentList();
    },
    handleSubmit: function(comment) {},
    render: function() {
        var fbDataHref = encodeFBCommentDataHref(this.props.uuid);
        var xfbmlComments = '<fb:comments notify="true" migrated="1" width="100%" href="' + fbDataHref + '"></fb:comments>';
        return (
            <div className="mdl-color-text--primary-contrast mdl-card__supporting-text">
                <div/>
                <div dangerouslySetInnerHTML={{
                    __html: xfbmlComments
                }} id="fbComments"/>

            </div>
        );
    }
});
//  <div className="fb-comments" data-href={fbDataHref} data-numposts="5" data-width="100%"/>
var Comment = React.createClass({
    render: function() {
        return (
            <div className={commentStyle.comment}>
                <div className={commentStyle.comment}>
                    <img/>
                    <span className="commentAuthor">
                        {this.props.author}
                    </span>
                </div>
                <span className={commentStyle.comment}>
                    {this.props.children}
                </span>
            </div>
        );
    }
});
var CommentList = React.createClass({
    render: function() {
        var comments = this
            .props
            .data
            .map(function(comment) {
                return <Comment author={comment.author}>{comment.comment}</Comment>;
            });

        return (
            <div className="commentList">
                {comments}
            </div>
        );
    }
});

var CommentForm = React.createClass({
    handleSubmit: function(e) {
        e.preventDefault();

        var authorDOM = React.findDOMNode(this.refs.author);
        var commentDOM = React.findDOMNode(this.refs.text);

        var commentText = commentDOM.value()
            .trim();
        var author = authorDom.value()
            .trim();
        if (!commentText || !author) {
            return;
        }

        authorDOM.value = '';
        commentDOM.value = '';
        var comment = {
            "uid": "",
            "u_imageBase64": "",
            "author": author,
            "comment": commentText
        };
        return;
    },
    render: function() {
        return (
            <div className="commentForm">
                <form>
                    <div className="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                        <textarea className="mdl-textfield__input" rows="3" type="text"></textarea>
                    </div>
                    <input className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" value="Post"/>
                </form>
            </div>
        );
    }
});
