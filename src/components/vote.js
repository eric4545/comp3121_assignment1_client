var VoteBox = React.createClass({
    getInitialState: function() {
        return {
            isLogined: false,
            isVoted: false,
            stat: {
                sad: 0,
                like: 0,
                shock: 0
            }
        }
    },
    calculateVoteStat: function() {
        var self = this;
        $.ajax({
            type: "GET",
            url: apiUrl.replace(':newsId', newsId)
                .replace('/vote', ''),
            success: function(data) {
                console.log(data);
                var votes = {
                    sad: 0,
                    like: 0,
                    shock: 0
                };
                data
                    .votes
                    .forEach(function(vote) {
                        if (votes[vote.feeling] === undefined)
                            votes[vote.feeling] = 0;
                        votes[vote.feeling]++;
                    });
                var length = data.votes.length / 100;

                if (votes.sad > 0)
                    votes.sad = Math.round(votes.sad / length);
                if (votes.like > 0)
                    votes.like = Math.round(votes.like / length);
                if ((votes.sad + votes.like) > 0)
                    votes.shock = 100 - votes.like - votes.sad;

                var state = self.state;
                state.stat = votes;
                self.setState(state);

                console.log(votes);
            }
        });
    },
    componentDidMount: function() {
        var self = this;
        self.calculateVoteStat();
        Object
            .observe(curUser, function() {
                if (curUser !== null) {
                    var state = self.state;
                    state.isLogined = (curUser.uid !== undefined && curUser.uid !== "") || false;

                    self.setState(state);
                }
            });
    },
    render: function() {
        console.log(this.state);
        var isLogined = this.state.isLogined || false;
        var isVoted = this.state.isVoted || false;
        var isActive = isLogined && !isVoted;

        return (
            <div className="mdl-grid">
                <div className="mdl-cell mdl-cell--4-col">
                    <Vote onvoted={this.calculateVoteStat} vote={{
                        icon: "😡",
                        feeling: "Sad",
                        stat: this.state.stat.sad || 0,
                        "isActive": isActive
                    }}/></div>
                <div className="mdl-cell mdl-cell--4-col">
                    <Vote onvoted={this.calculateVoteStat} vote={{
                        icon: "😍",
                        feeling: "Like",
                        stat: this.state.stat.like || 0,
                        "isActive": isActive
                    }}/></div>
                <div className="mdl-cell mdl-cell--4-col">
                    <Vote onvoted={this.calculateVoteStat} vote={{
                        icon: "😨",
                        feeling: "Shock",
                        stat: this.state.stat.shock || 0,
                        "isActive": isActive
                    }}/></div>
            </div>
        );
    }
});

var Vote = React.createClass({
    getInitialState: function() {
        return {
            stat: 0
        }
    },
    handleClick: function() {
        var self = this;
        var msg = {
            fbUid: curUser.uid,
            feeling: self.props
                .vote
                .feeling
                .toLowerCase()
        };
        $.ajax({
            type: "POST",
            url: apiUrl.replace(":newsId", newsId),
            data: msg,
            success: function(data) {
                self.props
                    .onvoted();
            }
        });
        console.log(msg);
    },
    handleSubmitVote: function(feeling) {},
    render: function() {
        var self = this;
        return (
            <div className="mdl-grid" style={{
                textAlign: "center"
            }}>
                <div className="mdl-cell mdl-cell--12-col">
                    <button className="mdl-button mdl-js-button mdl-button--icon mdl-button--colored" disabled={!this.props.vote.isActive} onClick={this.handleClick} style={{
                        width: 70,
                        height: 70
                    }}>
                        <i className="material-icons" style={{
                            fontSize: 50 , width: 50,
                              height: 50,transform: "translate(-24px,-20px)"
                        }}>{this.props.vote.icon}</i>
                    </button>
                </div>
                <div className="mdl-cell mdl-cell--12-col">
                    {this.props.vote.feeling}
                    <br/>
                    {this.props.vote.stat + " %"}
                </div>
            </div>
        );
    }
});

// <div>{this.props.vote.feeling}</div>
// <div>{this.props.vote.stat}</div>
// </button>
// <button className="mdl-button mdl-js-button mdl-js-ripple-effect">
//     <i className="material-icons">&#xE7F2;</i>
// </button>
// <button className="mdl-button mdl-js-button mdl-js-ripple-effect">
//     <i className="material-icons">&#xE7F3;</i>
// </button>
