var feedUrl = "http://www.ceo.gov.hk/eng/blog/rss/blog_rss.xml";
function encodeFBCommentDataHref(uuid) {
    return encodeURI(window.location.protocol + "//" + window.location.host + "/uuid=" + uuid);
}

function b64EncodeUnicode(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
}

class NewsContainer extends React.Component {
    constructor(props) {
        super(props);
        //getInitialState
        this.state = {
            news: {
                entries: []
            }
        };
        /*React components using ES6 classes no longer autobind this to non React methods.*/
        this.initializeFeed = this.initializeFeed
            .bind(this);
    }

    initializeFeed() {
        var feed = new google.feeds
            .Feed(feedUrl);
        feed.setNumEntries(1);
        var self = this;
        feed.load((function(result) {
            if (!result.error) {
                 $('meta[name=description]').attr('content', result.feed.entries[0].contentSnippet);
                self.setState({
                    news: result.feed
                });
            }
        }));
    }

    componentDidMount() {
        /*load feeds lib and call initializeFeed when finised loading*/
        google.load("feeds", "1", {
            "callback": this.initializeFeed
        });
    }
    render() {
        var news = this.state.news;
        var newsBox;
        if (news && news.entries && news.entries.length > 0)
            newsBox = <NewBox news={news.entries[0]}/>
        else {
            newsBox = <p className="mdl-spinner mdl-js-spinner mdl-spinner--single-color is-active"></p>
        }

        return (
            <div className="mdl-layout__content">
                <div className="mdl-grid">
                    <div className="mdl-cell mdl-cell--2-col"></div>
                    <div className="mdl-cell mdl-cell--4-col"></div>
                    {newsBox}
                </div>
            </div>
        );
    }
}
var NewsStyle = {
    container: {
        paddingLeft: "80",
        paddingRight: "80",
        paddingTop: "40",
        paddingBottom: "40"
    }
}

var NewBox = React.createClass({
    render: function() {
        var news = this.props.news;
        var newsUuid = b64EncodeUnicode(news.link);
        newsId = newsUuid;
        return (
            <div className="mdl-grid">
                {/*make follow to center*/}
                <div className="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
                <div className="mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">
                    <div style={NewsStyle.container}>
                        <div className="fb-login-button" data-auto-logout-link="true" data-max-rows="1" data-show-faces="true" data-size="medium" onlogin="checkLoginState();"
                           scope="public_profile,email,publish_pages,user_posts,publish_actions,manage_pages"/>
                        <h3>{news.title}</h3>
                        <article dangerouslySetInnerHTML={{
                            __html: news.content
                        }}></article>
                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--6-col">
                                <VoteBox/></div>
                            <div className="mdl-cell mdl-cell--6-col">
                                <KeywordBox uuid={newsUuid}/></div>
                        </div>
                        <CommentBox uuid={newsUuid}/>
                    </div>
                </div>
            </div>
        );
    }
});
