class KeywordBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keywords: ""
        };
        this.handleExtractComment
            .bind(this);
    }

    handleExtractComment() {
        extactFbCommentKeywords(encodeFBCommentDataHref(this.props.uuid), 5)
            .then(function(keywords) {
                keywords = keywords
                    .slice(0, 5)
                    .map(function(v) {
                        console.log(v);
                        return v.words;
                    });

                this.setState({
                    "keywords": keywords
                });

            }.bind(this));
    }

    componentDidMount() {
        if (this.props.uuid) {
            /*clone to above*/
            this.handleExtractComment();
            window.fbAsyncInit = () => {
                /*fire when comment related */
                FB.Event
                    .subscribe("comment.create", this.handleExtractComment);
                FB.Event
                    .subscribe("comment.remove", this.handleExtractComment);
            }
        }
    }

    render() {
        var keywordsList = <p className="mdl-spinner mdl-js-spinner mdl-spinner--single-color is-active"></p>;
        if (this.state.keywords) {
            keywordsList = this
                .state
                .keywords
                .map(function(words) {
                    return (
                        <li>{words}</li>
                    );
                });
        }
        return (
            <div>
                <div className="mdl-grid">
                    <div className="mdl-cell mdl-cell--12-col">
                        <h6>Top5 Keywords of All Comments</h6>
                        <ol>{keywordsList}</ol>
                    </div>
                </div>
            </div>
        );
    }
}

var fbGraphCommentApiPrefix = "https://graph.facebook.com/comments?id=";

function extactFbCommentKeywords(commentId) {
    var promise = new Promise(function(resolve, reject) {
        var commentUri = fbGraphCommentApiPrefix + commentId + "&limit=999999";
        var r = new XMLHttpRequest();
        r.open("GET", commentUri, true);
        r.onreadystatechange = function() {
            if (r.readyState != 4 || r.status != 200)
                return;
            try {
                var resJson = JSON.parse(r.responseText);
            } catch (err) {}
            if (resJson) {

                var keywords = {};
                resJson
                    .data
                    .forEach(function(comment) {
                        comment.message
                            .split(/[0-9\W]+/)
                            .forEach(function(word) {
                                keywords[word] = (keywords[word] || 0) + 1;
                            });
                    });
                var keywordsArr = [];
                for (var i in keywords) {
                    var v = {};
                    v.words = i;
                    v.counts = keywords[i];
                    keywordsArr.push(v);
                };
                keywordsArr = keywordsArr
                    .sort(function(a, b) {
                        return b.counts - a.counts;
                    });
                console.log(keywordsArr);
                resolve(keywordsArr);
            }
        };
        r.send();
    });
    return promise;
}

function extactKeyword(text, topCount) {
    var textArr = text.split(/[0-9\W]+/);
    var counts = {};
    textArr
        .forEach(function(x) {
            counts[x] = (counts[x] || 0) + 1;
        });
}
